<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('blog')->insert([
            'title'    => 'Virus corona sangat cepat menular',
            'content'  => 'jakarta makanan tradisional khas jawa pilihannya sangat beragam dan unik',
            'category' => 'Corona',
        ]);
    }
}
